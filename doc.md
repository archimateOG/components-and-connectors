# Introduction

The purpose of this document is to provide guidance in creating application landscape views from the runtime perspective. This is done by presenting “ready-to-go” constructs that can be used as idioms in a model composed of application components and connections among those components. The primary use of such an ArchiMate model would be to describe “as is” application landscapes and, in that way, provide the point of departure for the modeling of future architectures. 

Additionally, this document also formulates the modeling of some other aspects in this context, like the composition of components and relationships with datastores and infrastructure entities. 

The intention is to view application landscapes as systems and describe such systems in terms of components and their interconnections. That is needed because modern business processes are typically distributed systems incorporating multiple, independently developed application components that interact without human interposition. Having a model that accurately describes how the application components are interconnected is a necessity.

The presented constructs are intended for creating “component and connector” (C&C) views as set out by the [Software Engineering Institute (SEI)](https://www.sei.cmu.edu/) of Carnegie Mellon University in their "Views and Beyond" approach. By adhering to this approach we aim to ensure clarity and expressiveness. ArchiMate provides a limited set of broad concepts that are not meant to directly reflect what is observed in “as is” application landscapes. In dealing with this innate aspect of ArchiMate we needed an "intermediate" paradigm then would provide for bridging the distance between the ArchiMate concepts and the observed entities. Using the approach of SEI for that purpose is a natural choice because: a) it is an established part of the body of knowledge in the domain of architecture, and b) “component and connector” views are invented to provide for exploring architecture from the runtime perspective. A separate section elaborates further on the approach of SEI.

It is unlikely that architecture documents/repositories can provide for accuracy. Application interactions are nowadays coming into existence without being centrally architected and planned. Instead, enterprise application landscapes evolve, and the technology accelerates this trend by providing for ease of realization. As a result, application interconnections, especially those based on APIs (web services), proliferate out of sight of architects. 

Consequently, the information about interconnections is only available at the operational level. This, in its turn, limits what we can include in the model - only the information that can be reliably (and without a major effort) retrieved and verified should be used. For example, at the operation level, we can establish that applications are interconnected, which integration mechanisms are being used, and that interactions are taking place but, as a rule, we cannot determine what functionality is being provided and why. The modeling constructs proposed in this document are aligned with this limitation and are primarily intended to convey the information that can be observed in IT operations. 

Our criteria in proposing the C&C ArchiMate views are:
- Views are simple enough to comprehend unambiguously and require no additional explanations.
- The terminology of component and connector types is explicit and practical.
- The abstraction level of components and connectors appropriately hides implementation mechanisms.
- The ArchiMate constructs conform to the intended use of the language.


**Note:** _In this document, the term "application landscape" may denote not only the entirety of all business applications of an organization but also some subset of it. In other words, the modeling approach we describe does not necessarily need to include all applications._  

## The Intended Use

```
No more than 1 page, preferably 1/2 page
The added value
How to use it: for example, isolate components involved in a business process and use that portion as the starting point to show how the system works
- How components interact at runtime
- What components or services are replicated
- What components access data stores

A starting point for the analysis of runtime properties
- Performance
- Security
- Reliability

Maybe “use cases”: how this information is going to be used?
```



## “Component and Connector” Views



### What are the "component and connector" views?

The “Views-and-Beyond” approach to architecture documentation of [SEI](https://www.sei.cmu.edu/) categorizes architecture views in three so-called “viewtypes”. Each viewtype defines its set of element types and relationships to be used in describing the architecture from a specific perspective. One of the three perspectives is “component and connector” (**C&C**) in which we observe the units of runtime execution and interconnections among those units. The representation of a system (an architecture view) consists of two sorts of “first-class” elements: runtime _components_ (units of computation) and _connectors_ of various distinct types that enable and mediate interactions between components. Components interact with each other only via connectors which define how communication between components can take place. The C&C perspective can be used to describe any system or software architecture.

**_Connectors_** are characterized by their type and properties,  depending on the type. Each connector type also has a set of **_“roles”_** associated with it. A component interacting via a connector with another component must then take a specific role in that interaction. For example, in the case of publish/subscribe interconnections, a component can be attached to a “topic” connector by taking the role of either a “publisher” or a “subscriber.” 

**_Components_** are also characterized by their type and properties. Connectors are attached to components via components’ **_“ports.”_** Thus, when looking at a C&C view as a graph of interlinked components and connectors, a component’s connection-point is called a port, and a connector’s connection-point is called a role. 

![](./C-AND-C_General.png)

> _In summary_:
> 
> Elements of a component and connector model:
> - Components. Principal processing units and data stores (in our case, application components of an application landscape at the runtime). A component has a set of ports through which it interacts with other components via connectors.
> - Connectors. Pathways of interaction between components. Connectors have a set of roles that indicate how components use a connector in interactions. 
> 
> 
> Relations:
> - Attachments. Component ports are associated with connector roles to yield a graph of components and connectors.
> 
> Basic rules:
> - Components can only be attached to connectors, not directly to other components.
> - Connectors can only be attached to components, not directly to other connectors.
> - Attachments can only be made between compatible ports and roles.
> - Connectors cannot appear in isolation; a connector must be attached to a component.
> 
> _Quoted (with slight simplifications) from the [presentation “Chapter 18: Documenting Software Architectures”](https://people.eecs.ku.edu/~hossein/Teaching/Sp13/818/Lectures/SA-Practice/Chapter%2018.pptx ) (“Software Architecture in Practice, 3rd Edition” -Bass, Clements, Kazman)_



_Suggestions for further reading:_
- [Documenting Software Architectures: Views and Beyond, Second Edition](https://resources.sei.cmu.edu/library/asset-view.cfm?assetID=30386)
- [M. Show and others, "Abstractions for Software Architecture and Tools to Support Them," IEEE TRANSACTIONS ON SOFTWARE ENGINEERING, VOL 21, pp. 314-335, 1995](https://www.researchgate.net/publication/3187710_Abstractions_for_Software_Architecture_and_Tools_to_Support_Them)
- [S. Sarkar, "A Study of Existing Architecture Description Approaches from Enterprise System Development Perspective," 2004 (updated April 2016)](https://www.researchgate.net/publication/221610716_A_Study_of_Existing_Architecture_Description_Approaches_from_Enterprise_System_Development_Perspective)



### What connector abstractions are we going to use? 

> _Picking appropriate connector abstractions is often one of the most difficult jobs of producing effective architectural documentation using the C&C viewtype. If the connector abstractions are too low-level, then the view will become cluttered both with connectors and with components that are actually logically part of the connector mechanism (see below). If the connector abstractions are too high-level, it may be difficult to map from the C&C views to more implementation-oriented views. While deciding how abstract to make connectors is a matter of taste and the needs of architectural analysis, documentation that one finds for today’s systems tends to err on the side of being too low-level._ (**from**: [Documenting Software Architectures: Views and Beyond, Second Edition](https://resources.sei.cmu.edu/library/asset-view.cfm?assetID=30386))

The scope of the model we need to create is the entire application landscape. Accordingly, defining connectors in our case comes down to recognizing the commonly used interaction mechanisms or, in the terminology of the Enterprise Application Integration (EAI), “integration styles” (also called top-level integration patterns). In the proposed model, those integration styles are used to categorize the connector kinds. And, in the same way as we have chosen to use and rely on the C&C approach of the SEI (namely, to adhere to the existing body of knowledge), for this categorization we shall start with the patterns defined by[ G. Hohpe and B. Woolf in Enterprise Integration Patterns (Addison-Wesley, 2003)](https://www.enterpriseintegrationpatterns.com/). We have slightly adjusted this categorization to reflect the nowadays usage and product offerings.

> **_Remote Procedure Invocation_**
> 
> An application exposes interfaces of some of its procedures so that they can be invoked via network endpoints, and other applications can invoke those to run behavior and exchange data. The interaction takes place in real-time and synchronously. 
> By far most solutions nowadays are based on the HTTP protocol (SOAP and REST-like web services/APIs). The use of web services is fully supported in all application platforms (Java, .NET, Python, etc.) and requires no intermediate components.
> 
> **_Point-to-point Messaging (Queuing)_**
> 
> An application sends a message to a receiving application via a queue. An intermediary messaging system facilitates queuing and ensures that the message is made available to the recipient.
> 
> **_Publish/Subscribe_**
> 
> An application publishes an event to an intermediary messaging system using a message that addresses a “topic.” One or more other applications are subscribed to this topic and are notified of the event.
> 
> **_File Exchange_**
> 
> Files are moved from one file location to another; alternatively, a “producing” application creates a file in a shared directory. Most commonly, receiving applications monitor the file location for changes.
> 
> **_Shared Database_**
> 
> Multiple applications share the same physical database.
>
>(**from**:  [Accurate Insights into the Runtime Topology of Application Landscapes](https://eapj.org/accurate-insights-into-the-runtime-topology-of-application-landscapes/))

For these integration patterns, we define the following "connectors":

| Connector Type (supported interactions) | Associated real-life Entity | Possible roles that can be taken by an attached component |
| --- | ----------- | ----------- |
| **API** (Remote Procedure Invocation Style): Synchronous APIs (web services), possibly all based on HTTP. Encompasses WSDL/SOAP services, REST-like APIs, gRPC, etc. | Web service (API) or endpoint | Service provider (1), Service consumer (multiple)|
|**Queuing** (Point-to-point messaging via queues)|Queue|Message producer (multiple), Message consumer (1) |
|**Topic** (Publish/subscribe)|Topic|Publisher (multiple), Subscriber (multiple) |
|**File Exchange**: FTP file transfers, shared directories|Directory or something else (?)|Attached (multiple), alternatively: Exporting (multiple), Importing (multiple) |
|**Shared Database**|Database|Attached (multiple), alternatively: Writing (multiple), Reading (multiple) |
|**Other**: Any other integration option (presumably a “legacy”)||Attached (multiple), alternatively: Source, Sink  |


### What will "components" be representing? 

Obviously, one would expect that a component in a view should be representing a real-life application component, in the way as it has been originally intended. However, this may be an issue when dealing with many (service oriented) interactions across the organization boundaries. If an internal application consumes an API provided by another organization, there is no knowledge about the component that exposes that API. The agreements about such interactions are made about the provided and consumed services, and the actual components of the other party are completely out of sight.  Our C&C view may show such components as we know they exist and, in this guidance we choose to do so. Yet it remains somewhat artificial to include in a view the components that are completely unknown (apart from being service consumers or providers). A similar situation arises with microservices, where the premise is that one component provides one service.

This issue requires a separate discussion. Further in this text, we choose that components in C&C always represent some kind of a real-life application component.

However, we have to take the following into the consideration:
- When describing "as is" application landscapes in terms of components and connectors, the managed interconnections demarcate the components. By saying "managed" we emphasize that not all interconnections are visible; there may be smaller grained application components embodied in larger components but not visible as such. In other words, some compositions of application components may not be visible in our views. Consequently, our model is intermixing components that are otherwise perceived as being at a different composition level.
- To be complete, our model needs to show all interconnections which also includes those that support interactions across the organization's boundaries. Consequently, we need to show how the external applications are connected which also implies that we should show those external applications. For example, if an internal application exposes a service to an external application, we choose to include that external application in our model (and subsequently, in our C&C views).


To summarize (through examples), components in **_our_** C&C views could represent:
- _**An enterprise application component**_ deployed either on-premises or in a managed cloud infrastructure environment (IaaS, PaaS), as identified by its business owner. In most organizations, these components will have the highest presence;
- _**A SaaS application**_  - Although we know that SaaS solutions are not monoliths, their inner structure is beyond the knowledge and concern of the organization using it;
- _**An external application**_ authorized to interact with the enterprise’s applications by providing or consuming web services, being message queue producer or consumer … (external means that it is owned by another organization). 
- _**An entire (partner) organization**_ - For example, enterprise publishes selected APIs to partners – often we do not know which concrete application components consume exposed services and we equally don’t care – it is outside of the enterprise, beyond our concern … In this case, when specifying external components, we may aggregate all these to one (substitute) component;
- _**All anonymous consumers of an API with unrestricted access**_ - One component stands for multiple unidentifiable applications)
- _**A uniformly managed set of handheld or browser-based applications or information kiosk applications**_  - One component stands for multiple runtime instances of an application;
- ...



# Proposed modeling constructs
## Principles, assumptions, and constraints

* * * 
- **What is our concrete objective?**

We want to create ArchiMate models from the observed reality that are suitable for visualizing C&C views. For expressiveness and clarity,
the ArchiMate model should stay close to that reality. (thus, to be understandable on the operational level and not only to enterprise architects) 
Specifically, this is to say that ArchiMate models should not be representing “connectors” in general, but rather the concrete types of interconnections (connector types), as listed in the table with Connector Types in the section ["What connector abstractions are we going to use?"](#what-connector-abstractions-are-we-going-to-use).


* * *
- **What is our approach?**

The first step is to define a (non-ArchiMate) meta-model describing all interconnections that is also suitable to be reduced to a graph of components and connectors. The intention is to define abstractions suitable for creating models that would resemble real-life entities as close as possible while having regard for the limitations in observing the actual application landscape. We will not be formally defining this meta-model; instead, we are using illustrative examples for each connector type separately. Subsequently, we explore the possibilities of representing these examples in ArchiMate – we call those ArchiMate representations the “example models”. 

_Notice that, for clarity reasons, the example models are intentionally reduced to include only the entities that may be a part of a C&C view. [A separate section of this document](https://gitlab.com/archimateOG/components-and-connectors/-/blob/master/doc.md#relation-with-other-viewpoints) describes how to include other entities (that in real-world practice are also expected to be a part of a model)._   

The last step is showing how to derive C&C views from example models. In doing that, we do not apply any model transformations – the example model may only be reduced by not including some elements and relationships. 


![](./process.png)



* * * 
- **Which ArchiMate Layer?**

It is self-evident that components and connectors should be represented in the Application Layer as much as possible.
* * * 

- **How to represent "components"?**

It is equally evident that we should use the Application Component element to represent components. 
* * * 
- **Structural vs Behavioral elements?**

Regarding the use of structural vs. behavioral elements of ArchiMate for representing interconnections: 
Our model shows the structure of an application landscape; therefore, Intuitively, one may expect that ArchiMate representations should include only the structural elements. However, that would make the ArchiMate modeling less effective. Although we are dealing with interconnections (which stand for a structure), interconnections are there for a purpose, namely, to facilitate interactions. In the C&C approach, a connector primarily describes the behavior of the interaction it mediates. Accordingly, the preference is to use an ArchiMate behavioral element or an active structure element, if possible (?) 

* * * 
- **C&C views are broadly defined. How to get most of it in our context?**

C&C views are broadly defined so that they may be used to show the architecture of any system. The first step in using the approach of “Views and Beyond” should be narrowing down the available choices to our concrete needs. As outlined in the preceding paragraphs, we intend to represent application components as they are identified by observing their managed connections (or, in other words, the potential interactions with other application components). We also intend to have connectors representing the mainstream interaction mechanisms. This leads to the following reductions:
- We will not use C&C ports. The abstraction “port” is introduced to specify different attachment points on the component element. That has no meaning in our case – we can assume that each attachment has its own, exclusive port.

- The semantics of C&C roles will be assigned to attachments (relation between components and connectors). The ArchiMate relationship will then denote the role. That can be done either by using different relation types or, in cases where we can have only two roles, by using the direction of the relation. This is illustrated with the following example that shows how to represent three applications interacting via a topic.
![](./principles_pub-sub_example.png)

* * * 

- **Should we visualize connectors with connecting lines  or with "in-between" boxes?**

C&C views may visualize connectors as a line  or as a box (showing connectors in a graph either by nodes or by edges). We assume that a box representation would be more suitable for most uses. We will thus primarily elaborate on how to represent connectors with ArchiMate elements (rather than with ArchiMate relationships). As in some cases one would prefer showing a connector with a line (for example, when one wants to focus on dependencies between applications in general) we will also discuss direct ArchiMate-relationships between interacting components.

* * *

- **Should we uniformly represent all interconnections?**

API interconnections are most commonly service-oriented, while all others are data-oriented. Accordingly, representing API-connectors differently than the rest would improve the expressiveness. There is not much value in striving for uniformity and representing all connectors in the same way.


* * * 
- **Can our model include more information (extending C&C), and, if yes, how to deal with it?**


- The model representing the observed reality should include additional information if it is available. It may therefore also include additional elements, not only those representing components and connectors. Moreover, the model is intended to be a departing point for further modeling, therefore it should be accomodating for more information. However, the semantics of C&C must be preserved. This is to say, for example, that additional elements shouldn’t be in-between a component and a connector. In other words, the direct relation between components and connectors (that bears the semantics) must be preserved. 
- Not all information pertinent to application interconnection is suitable for ArchiMate representation, nor we need to have it stored in a modeling tool. (for example, some details like OpenApi or WSDL definitions) Instead, we should use links to switch from a modeling tool (ArchiMate) to another information source.
- We may use properties to convey more information.


## 🏷️Remote Procedure Invocation (API/Web Service)
The intention is to represent all synchronous (request-response) web services/APIs, as a rule communicating via HTTP, with the same construct. This encompasses WSDL/SOAP services, REST-like APIs, gRPC, etc. that are all entirely supported in application platforms and do not require intermediate units. API Gateways that are used to facilitate these interactions are optional and will not be included in the C&C view. Possibilities how to yet denote the presence of these intermediaries are discussed separately. 

The ArchiMate representations will be discussed based on a hypothetical example showing one application component exposing two web services, and four application components consuming those. In that example, we also show how an API could be exposed on more than one endpoint.


![](./API_ER.png)

#### API Example Model A
The first step is to model the observed entities in ArchiMate one-on-one to create an "example model" - We shall refer to that model as "_**API Example Model A**_" (later, we shall add one more example model):
- We have used the Application Service element to represent web services (APIs) and the Application Interface element to represent endpoints. 
- The service providing Application Component _Realizes_ _Application Services_.
- The consuming relationships between Application Components and Endpoints have been modeled as _Serving_ relationships.
- Application Interfaces (Endpoints) are assigned to Application Services they expose.
- _Composition_ relationships associate Endpoints with the _Application Components_ that provide Services they are assigned to.

In addition, for the purpose that will be elaborated later, we have also defined two more _Serving_ relationships (that would be otherwise derivable):
- Application Services _Serve_ service consuming Application Components (shown in blue).
- Service providing Application Component _Serves_ service-consuming Application Components (shown in red).


![](./API_B1.png)


In terms of C&C (ignoring for the time being the relationships shown in red and blue), we have two elements (Services and Interfaces) denoting the attachment between interacting components. This means that views including all elements would not comply with the C&C basic rules, so we need to reduce the number of elements (one may consider Application Services to be the C&C "ports", but that would make views less intelligable when showing more than just a few Application Components. Moreover, Endpoints are, in general, beyond the architectural concerns). Therefore, in our views we shall implicitly reassign the semantics of real-life entities to ArchiMate elements as needed. We have identified several options for doing that.

### C&C View Option 1 for APIs

Here, we use arrows (ArchiMate relations) to represent interconnections. This representation is suitable for showing dependecies between application components. 

![](./API_B1_1.png)


### C&C View Option 2 for APIs

Interconnections are represented with Application Service elements. The application component that exposes the API is attached with the Realization relation (it is a service provider) and the application that invokes the API is attached with the Serving relation.

![](./API_B1_2.png)
_**Ratio:**_ In many cases, application services are implemented through web services, literally one-on-one. This is not surprising as the most important web service standard, WSDL, has been defined in accordance with the Service Oriented Architecture. Indicating such interconnections using the Application Service element is a logical choice.

This C&C view can be derived from the _base model_ by omitting the Application Interface elements. 


![](./API_Opt1_trans.png)


#### PROS 
- Expressive and easy-to-understand
- Emphasizes service orientation.
- This representation is often used in examples

#### CONS 
- May give the impression of implying that there have to be a one-on-one relationship between APIs (observed at runtime) and Application Services (in ArchiMate models)
- The information over endpoints is not included in this view.


### C&C View Option 3 for APIs
Interconnections are represented with Application Interface elements. The application component that exposes the API (the service provider) is attached with the Composition relation and the application that invokes the API (the service consumer) is attached with the Serving relation. Each connector (Application Interface element) is associated with one endpoint. 

![](./API_B1_3.png)

_**Ratio:**_ Option 2 has been invented for the following reason: If APIs (represented by Application Service elements) appear together with some other Application Services (that are modeled by architects) in the same model, one would like to differentiate between the two varieties. If Application Interface elements are otherwise less used, they are more suitable to represent APIs than Application Service elements.


![](./API_Opt2B_trans.png)
 
#### PROS 
- Equally expressive and easy-to-understand as Option 1.
- Emphasizes interfaces.
#### CONS 
- May trigger a discussion about the meaning of an "interface". The Composition relation means that the API 'is-a-part-of' the service providing application. However, API-endpoints (which, in ArchiMate, belong to interfaces) are commonly exposed on API Gateways and that may be confusing.
- Semantics of the API is implicitly associated with each endpoint, therefore the same semantics appears in two elements.
- Endpoints are mostly of no concern for architects 


### C&C View Option 4 for APIs
Much like in **Option 3** nterconnections are represented with Application Interface elements. The application component that exposes the API (the service provider) is attached with the Composition relation and the application that invokes the API (the service consumer) is attached with the Serving relation. In this case, each connector (Application Interface element) is associated with the API (the provided service) and the information concerning the individual endpoints is not conveyed. 
![](./API_B2_4.png)

#### PROS 
- Equally expressive and easy-to-understand as Option 1.
- Emphasizes interfaces.
#### CONS 
- May suggest associating the semantics of a service with an Application Interface element.


#### API Example Model B
This view cannot be based on "_**API Example Model A**_" without a transformation. If a service is being exposed via more than one endpoint, we need to derive an Application Interface element by combining these endpoints. We shall refer to that model as "_**API Example Model B**_". 
![](./API_Opt2A_trans.png)


### API Connectors: Alternatives considered and abandoned 
None worth mentioning.


## 🏷️Point-to-point Messaging (Queuing) and 🏷️Publish/Subscribe (Topics)
_Interconnections based on queues and those based on topics can be represented in the same way. To avoid duplicating texts we elaborate on those connectors in the same section._

The elaboration is based on an example including two queues and one topic.

![](./Q_T_Actual_ER.png)

* * * 
Separate "example models" for each option.
* * * 

#### Queues and Topics Example model A

![](./Q_T_Base_1.png)

* * * 

#### Queues and Topics Example model B

![](./Q_T_Base_2.png)

* * * 

### C&C View Option 1 for Queues and Topics

Can be derived from all three example models

![](./Q_T_on_2.png)

* * * 

### C&C View Option 2 for Queues and Topics

![](./Q_T_on_B1.png)

* * * 

### C&C View Option 3 for Queues and Topics

![](./Q_T_on_B2.png)

* * * 

### Queues and Topics Connectors: Alternatives considered and abandoned 

![](./Q_T_Base_3.png)




## 🏷️Shared Database
`the type of access (read / write) is known`
![](./SharedDB.png)
`the type of access (read / write) is unknown`
![](./SharedDB_undirected.png)

## 🏷️File Exchange
`the type of access (read / write) is known`
![](./File_Exchange.png)
`the type of access (read / write) is unknown`
![](./File_Exchange_undirected.png)

## 🏷️Other (Legacy, Custom ...)
`possibly more`

# Visualization Considerations
![](./TG1.png)
# Relation with other Viewpoints
## Choices
### Relating "API Connectors" to other entities
![](./API_other.png)
### Relating "Queue Connectors" and "Topic Connectors"to other entities
`This is one possiblity ... I'm sure it can be done better and I'm not sure if this is correct at all ... `
![](./Q_T_other.png)
#### Relating the "Shared Database Connector" to other entities
#### Relating the "File Exchange Connector" to other entities
## Context Diagram
## Application Structure
## Technology Realization
## Business Layer Support
## Capability Realization
## Data Flow
## Control Flow

# Reusability & Extendability
`simplify -- extend -- relate`

# Maintainability

## Data Input / Syncronization With Other Sources
```
* manual
* analyse in runtime
* config static analysis?
```

