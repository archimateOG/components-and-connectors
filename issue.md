This issue is a proposal for Work Group to define and propose a set of modelling construct options based on ArchiMate to be used directly
or serve as guidance in creating application landscape views from the runtime perspective.

## Motivation
Having insights into the actual state of the application landscape is a prerequisite for planning a future Enterprise Architecture. A critical part of these insights is knowing how the application components are interconnected at the runtime. The modern business processes are typically supported by distributed systems incorporating multiple, independently developed application components that interact without human interposition.
Having a model that accurately describes how the runtime entities may interact and what are the pathways of
interactions are essential for adequate and correct execution of Architecture Decision and Analysis processes.

Often this information is available in different systems such as CDMB or monitoring solutions. It is, however, very
rarely used effectively with a combination of Architecture Models using standard modelling languages such as ArchiMate.

The security aspect is also an essential element of Software Architecture. Such information is often modelled within an
Architecture Model, but some part also available in runtime configurations. Combining this relevant information into a single model would help for more accurate security and threat analysis.

Decision recording is vital for evaluating the architecture and solution quality using various Quality Attributes.
Runtime information provides the metrics for assessment of those Quality Attribute Scenarios and thus helps to make the
right Architecture Decisions.

It is not the purpose of Architecture Models to aggregate all this detailed data; however, there should be a mapping of some concepts between the information from the runtime perspective and the architecture models created.

In this work, we want to document the runtime structure of an application landscape in ArchiMate. There are many possibilities to model that, and practice clearly shows the many variations and interpretations.
Consequently, those ArchiMate models are too often understood only by architects that have been involved in their creation.
It should not be so that the needed modelling constructs are reinvented all over again. We discuss the option to have a set of predefined modelling constructs, more like a meta-model based on ArchiMate, used as idioms in models composed of application components and connections.


Often accurate information about interconnections is only available at the operational level and not easily accessible, limiting what we can include in the model - only the information that can be reliably (and without significant efforts) retrieved and verified should be used. For example, we can establish that applications are interconnected at the operation level, which integration mechanisms are being used, and that interactions are taking place. As a rule, we cannot determine what functionality is being provided and why. The idiomatic modelling constructs we are after should be aligned, respecting this limitation.

When examining the runtime structure of application landscapes, we need simple enough views to comprehend unambiguously and require no additional explanations. We believe that the views describing the runtime structure should be aligned with "component and connector" views as set out by the [Software Engineering Institute (SEI)]
(https://www.sei.cmu.edu/) of Carnegie Mellon University in their "Views and Beyond" approach. Adherence to this approach would ensure clarity and expressiveness. ArchiMate provides a limited set of broad concepts that are not intended to directly reflect what is observed in "as is" application landscapes. A solution would be to introduce an intermediate paradigm. That paradigm would provide additional abstractions in bridging the distance between the ArchiMate concepts and the observed entities. Using the approach of SEI for that purpose would be a natural choice because: a) it is an established part of the body of knowledge in the domain of architecture, and b) "component and connector" views are invented to provide for exploring architecture from the runtime perspective.

We cannot use just one modelling construct for all varieties of interconnections. We need to categorize them first and use the most appropriate modelling construct for each category (i.e. connector type). Here we should also prefer to rely on the established body of knowledge - we propose to use integration styles (EAI) as connector types.

Views created in accordance with the above ideas may look something like this:

![](./TG1.png)

* * * 
* * * 

or like these:

![](./TG1A.png)

 * * * 
 
 * * * 

![](./TG1B.png)

 * * * 
 ...
 * * * 
Or simply link that:

![](./TG3.png)

 * * * 
 ...
 * * * 


To summarize, we believe that it would be of a substantial benefit to have two documents concerning the modeling of the runtime structure of the "as is" application landscapes:
- an exhaustive, “living” document which would elaborate on the issue in detail and describe possible modeling alternatives including the rationale, pros and cons, etc.
- a quick reference with just a list of the preferable modeling constructs for each connector type (a kind of “cheat sheet”).

This issue supecedes the issues [#15](https://community.opengroup.org/archimate-user-community/home/-/issues/15) and [#16](https://community.opengroup.org/archimate-user-community/home/-/issues/16).

